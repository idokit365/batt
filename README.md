# Batt

A simple app that shows battery status information on Android devices. **Requires Android 14.**

_Featured by [Mishaal Rahman](https://twitter.com/MishaalRahman/status/1664340667525373952), [Android Central](https://www.androidcentral.com/apps-software/android-14-battery-health-insights), [GSMArena](https://m.gsmarena.com/android_14_battery_health_feature-news-58764.php) and [Android Authority](https://www.androidauthority.com/android-14-battery-health-check-3331255/)._

<img height="800" src="screenshot.png"></img>

## Installation

Install it from the [latest release](https://gitlab.com/narektor/batt/-/releases).

## What Batt displays

By default, it shows:
- the number of charge cycles
- the charge status

However, with some setup,, it can also show:
- the battery health
- the battery manufacturing date
- the battery first use date

For this to work, either use Shizuku (instructions can be found in the app) or the following ADB command:

```
adb shell pm grant com.porg.batt android.permission.BATTERY_STATS
```

## Technical

The app is built using the traditional View-based system and Material 3 components. To get the `BATTERY_STATS` permission it uses the [Shizuku API](github.com/RikkaApps/Shizuku-API).

Make sure to use Android Studio Giraffe or later.

## License
Batt is licensed under the GNU GPL, version 3.

## Acknowledgements
- [Android 14 API Tests](https://gitlab.com/android-api-tests/upside-down-cake) for the base
- [Mishaal Rahman](https://t.me/MishaalAndroidNews/490) for finding and reporting on the new APIs
