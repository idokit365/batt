package com.porg.batt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.BatteryManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.card.MaterialCardView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.progressindicator.LinearProgressIndicator
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.Calendar
import java.util.Date
import java.util.Locale

class MainActivity : AppCompatActivity() {

    private var HIDDEN_CARDS = mutableListOf<View>()
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            updatePublicData(intent)
            updateHiddenData()
        }
    }

    private var compatDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // If the device is incompatible, show a warning
        if (CompatChecker.isIncompatible(applicationContext)) {
            val builder = MaterialAlertDialogBuilder(this)
            builder.setMessage(getString(R.string.dlg_compat_desc))
                .setTitle(getString(R.string.dlg_warning_title))
                .setPositiveButton(getString(android.R.string.ok)) { _, _ -> compatDialog?.hide() }
                .setCancelable(true)
            compatDialog = builder.create()
            compatDialog!!.show()
        }

        // Receive the current battery status
        val possibleBatteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            this.registerReceiver(receiver, ifilter, RECEIVER_NOT_EXPORTED)
        }
        if (possibleBatteryStatus == null) {
            Toast.makeText(this, "Cannot get battery data", Toast.LENGTH_SHORT).show()
            finish()
        }
        // This is to get lint to shut up,
        // it's pretty much guaranteed to not be null as the activity finishes if it is
        val batteryStatus = possibleBatteryStatus!!

        updatePublicData(batteryStatus)

        // Check if we have battery stats permission
        if (ContextCompat.checkSelfPermission(this, BATTERY_STATS_PERM) == PackageManager.PERMISSION_GRANTED) {
            findViewById<MaterialCardView>(R.id.perm_request).visibility = View.GONE

            // Enable extra features
            findViewById<LinearLayout>(R.id.extra_health).visibility = View.VISIBLE
            findViewById<MaterialCardView>(R.id.extra_mfc_date).visibility = View.VISIBLE
            findViewById<MaterialCardView>(R.id.extra_use_date).visibility = View.VISIBLE
            findViewById<MaterialCardView>(R.id.extra_charge_policy).visibility = View.VISIBLE

            updateHiddenData()
        } else {
            findViewById<Button>(R.id.perm_request_button).setOnClickListener {
                startActivity(Intent(this, PermGrantActivity::class.java))
            }
        }

        hideCards()
    }

    private fun updateHiddenData() {
        if (ContextCompat.checkSelfPermission(this, BATTERY_STATS_PERM) != PackageManager.PERMISSION_GRANTED) return
        // Get battery manager
        val mBatteryManager = this.getSystemService(BatteryManager::class.java)
        // Get battery capacity
        val capacity = mBatteryManager.getIntProperty(BATTERY_PROPERTY_STATE_OF_HEALTH)
        findViewById<TextView>(R.id.extra_health_desc).text = String.format(getString(R.string.extra_capacity_desc), capacity)
        findViewById<TextView>(R.id.extra_health_title).text = when {
            capacity >= 80 -> getString(R.string.battery_health_good)
            capacity in 31..79 -> getString(R.string.battery_health_mid)
            capacity < 30 -> getString(R.string.battery_health_bad)
            else -> getString(R.string.battery_health_unknown)
        }
        findViewById<ImageView>(R.id.extra_health_image).setImageResource(when {
            capacity >= 80 -> R.drawable.ic_health_good
            capacity in 31..79 -> R.drawable.ic_health_mid
            capacity < 30 -> R.drawable.ic_health_poor
            else -> R.drawable.ic_health_unknown
        })
        val capacityBar = findViewById<LinearProgressIndicator>(R.id.battery_progress)
        capacityBar.setIndicatorColor(getColor(when {
            capacity >= 80 -> R.color.my_light_primary
            capacity in 31..79 -> R.color.yellow
            capacity < 30 -> R.color.red
            else -> R.color.white
        }))
        capacityBar.progress = capacity
        // Hide the card if the capacity is incorrect
        if (capacity > 100 || capacity < 0) {
            HIDDEN_CARDS.add(findViewById(R.id.extra_health))
        }

        // Get manufacturing date
        val manufacturingDate = mBatteryManager.getLongProperty(BATTERY_PROPERTY_MANUFACTURING_DATE)
        Log.d("batt", manufacturingDate.toString())
        findViewById<TextView>(R.id.extra_mfc_text).text = toDateString(manufacturingDate)
        // Hide the card if the manufacturing date is incorrect
        if (!dateIsValid(manufacturingDate)) {
            HIDDEN_CARDS.add(findViewById(R.id.extra_mfc_date))
        }

        // Get first use date
        val usageDate = mBatteryManager.getLongProperty(BATTERY_PROPERTY_FIRST_USAGE_DATE)
        Log.d("batt", usageDate.toString())
        findViewById<TextView>(R.id.extra_use_text).text = toDateString(usageDate)
        // Hide the card if the use date is incorrect
        if (!dateIsValid(usageDate)) {
            HIDDEN_CARDS.add(findViewById(R.id.extra_use_date))
        }

        // Get charging policy
        val chargingPolicy: Int = mBatteryManager.getIntProperty(BATTERY_PROPERTY_CHARGING_POLICY)
        findViewById<TextView>(R.id.extra_charge_policy_text).text = when (chargingPolicy) {
            CHARGING_POLICY_DEFAULT -> getString(R.string.battery_charge_policy_default)
            CHARGING_POLICY_ADAPTIVE_AC -> getString(R.string.battery_charge_policy_ac)
            CHARGING_POLICY_ADAPTIVE_AON -> getString(R.string.battery_charge_policy_aon)
            CHARGING_POLICY_ADAPTIVE_LONGLIFE -> getString(R.string.battery_charge_policy_longlife)
            else -> getString(R.string.battery_health_unknown)
        }
    }

    private fun updatePublicData(batteryStatus: Intent) {
        // Show cycle count
        var cycles: String = batteryStatus.getIntExtra(EXTRA_CYCLE_COUNT, -1).toString()
        if (cycles == "-1") cycles = "???"
        findViewById<TextView>(R.id.battery_cycles).text = cycles

    }

    private fun dateIsValid(date: Long): Boolean {
        // Reject default dates instantly
        if (date <= BATTERY_USAGE_DATE_IN_EPOCH_MIN || date >= BATTERY_USAGE_DATE_IN_EPOCH_MAX) return false
        // Reject dates in the future
        val now = Instant.now().epochSecond
        return date < now
    }

    @Deprecated("Replaced with dateIsValid (the output is inverted)")
    private fun failsDateCheck(date: Long): Boolean {return !dateIsValid(date)}

    private fun hideCards() {
        for (card in HIDDEN_CARDS) {
            card.visibility = View.GONE
        }
        if (HIDDEN_CARDS.isNotEmpty()) {
            findViewById<MaterialCardView>(R.id.missing_data).visibility = View.VISIBLE
            findViewById<MaterialCardView>(R.id.missing_data).setOnClickListener {
                for (card in HIDDEN_CARDS) {
                    card.visibility = View.VISIBLE
                }
                findViewById<MaterialCardView>(R.id.missing_data).visibility = View.GONE
            }
        }
    }


    fun toDateString(time: Long): String {
        val date = Date(time*1000)
        val calendar: Calendar = Calendar.getInstance()
        calendar.time = date
        // Convert to yyyy-MM-dd HH:mm:ss format string
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        return sdf.format(date)
    }
}